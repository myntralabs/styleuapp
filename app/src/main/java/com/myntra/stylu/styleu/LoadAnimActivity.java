package com.myntra.stylu.styleu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;


public class LoadAnimActivity extends AppCompatActivity implements Animation.AnimationListener {

    CircleImageView circularImageView, circularImageView1;
    Animation myAnim;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        circularImageView = (CircleImageView) findViewById(R.id.yourCircularImageView);
        circularImageView1 = (CircleImageView) findViewById(R.id.yourCircularImageView1);
        if (!CameraActivity.file.equals("")) {
            File file = new File(CameraActivity.file);
            Picasso.with(this).load(file).into(circularImageView, new Callback() {
                @Override
                public void onSuccess() {
                    didTapButton();
                }

                @Override
                public void onError() {
                }
            });
        }

//        preferences= getSharedPreferences("pref", 0);
//        String filepath = preferences.getString("takenimg", "");


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(LoadAnimActivity.this, ScoreActivity.class);
                startActivity(i);
                overridePendingTransition( R.anim.slideup_activity, R.anim.slidedown_activity );

                finish();
            }
        }, 3000);
    }

    public void didTapButton() {
        myAnim = AnimationUtils.loadAnimation(this, R.anim.blinkanim);
        myAnim.setRepeatCount(Animation.INFINITE);
        circularImageView1.startAnimation(myAnim);


    }

    public void onAnimationEnd(Animation animation) {
        didTapButton();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
        circularImageView1.startAnimation(myAnim);
    }

}
