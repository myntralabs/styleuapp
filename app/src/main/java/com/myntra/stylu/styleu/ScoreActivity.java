package com.myntra.stylu.styleu;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScoreActivity extends AppCompatActivity {

    TextView scorecomment;
    Button sharebutton;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat_Regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );

            setContentView(R.layout.activity_score);

            scorecomment= (TextView) findViewById(R.id.message);
            sharebutton= (Button) findViewById(R.id.share);

            sharebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    Uri screenshotUri = Uri.parse("");

                    sharingIntent.setType("image/png");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                    startActivity(Intent.createChooser(sharingIntent, "Share image using"));
                }
            });


            StyleSpan styleSpan=new StyleSpan(Typeface.BOLD);

            scorecomment.setText(colorString(styleSpan,
                    "Great job, you are wearing a striped shirt with solid chinos for a casual look","striped shirt" +
                            "", "solid chinos" + "","casual look"));
        }

        public static SpannableString colorString(StyleSpan span, String text, String... wordsToColor) {
            SpannableString coloredString = new SpannableString(text);

            for(String word : wordsToColor) {
                int startColorIndex = text.indexOf(word);
                int endColorIndex = startColorIndex + word.length();
                coloredString.setSpan(CharacterStyle.wrap(span), startColorIndex, endColorIndex,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            return coloredString;
        }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
      }

    }
