package com.myntra.stylu.styleu;

/**
 * Created by 300012407 on 03/10/17.
 */

public class Badge_view {

    private String name;
    private int badgeName;
    private int thumbnail;

    public Badge_view() {
    }

    public Badge_view(String name, int badgeName, int thumbnail) {
        this.name = name;
        this.badgeName = badgeName;
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}

