package com.myntra.stylu.styleu;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.myntra.stylu.styleu.R.layout.gallery;

public class CameraActivity extends AppCompatActivity implements Callback,
        OnClickListener {

    private static final String TAG = "Camera";
    public static String file;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private static Camera camera;
    private Button flipCamera, flashCameraButton, captureImage, profileImage;
    private ImageButton saveButton, cancleButton;
    private int cameraId, rotation;
    private boolean flashmode = false;
    View saveButtonLayout, captureButtonLayout;
    byte[] takenpicture;
    RelativeLayout baseRelativeLayout;
    ImageView capturedImageView;

    ArrayList<String> list;
    private SlidingUpPanelLayout slidingLayout;
    GridView sdcardImagesGridView;
    RelativeLayout t1;
    TextView titleTextView;
    boolean isCaptured = false;
    FrameLayout dragView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        t1 = (RelativeLayout) findViewById(R.id.t1);
        titleTextView = (TextView) findViewById(R.id.title);
        t1.setVisibility(View.GONE);
        titleTextView.setVisibility(View.GONE);
        list = ImagesFromDevice.getAllShownImagesPath(this);
        dragView= (FrameLayout) findViewById(R.id.dragView);
        sdcardImagesGridView = (GridView) findViewById(R.id.sdcard);
//        sdcardImagesGridView.setVerticalScrollBarEnabled(false);
        sdcardImagesGridView.setEnabled(false);
        sdcardImagesGridView.setAdapter(new ImageAdapter(this));
        sdcardImagesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                final Animation slideUpAnimation=AnimationUtils.loadAnimation(CameraActivity.this, R.anim.slide_up);
                capturedImageView.setImageBitmap(null);
                capturedImageView.setVisibility(View.VISIBLE);
                Glide.with(CameraActivity.this).load(list.get(position)).listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        saveButtonLayout.setVisibility(View.VISIBLE);
                        capturedImageView.setVisibility(View.VISIBLE);
                        slidingLayout.setVisibility(View.GONE);
                        releaseCamera();
                        saveButtonLayout.setAnimation(slideUpAnimation);
                        file=list.get(position);
                        return false;
                    }
                }).into(capturedImageView);



            }
        });
        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        //some "demo" event
        slidingLayout.setPanelSlideListener(onSlideListener());

        //camera

        capturedImageView = (ImageView) findViewById(R.id.i);
        baseRelativeLayout = (RelativeLayout) findViewById(R.id.base);
        saveButtonLayout = findViewById(R.id.saveButtonLayout);
        captureButtonLayout = findViewById(R.id.captureLayout);

// camera surface view created
        cameraId = CameraInfo.CAMERA_FACING_BACK;
        profileImage = (Button) findViewById(R.id.profile);
        flipCamera = (Button) findViewById(R.id.flipCameraButton);
        flashCameraButton = (Button) findViewById(R.id.flashButton);
        captureImage = (Button) findViewById(R.id.captureButton);
        saveButton = (ImageButton) findViewById(R.id.save);
        cancleButton = (ImageButton) findViewById(R.id.cancel);
        surfaceView = (SurfaceView) findViewById(R.id.preview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.setFormat(ImageFormat.JPEG);
        surfaceHolder.addCallback(this);
        flipCamera.setOnClickListener(this);
        captureImage.setOnClickListener(this);
        flashCameraButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        cancleButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (Camera.getNumberOfCameras() > 1) {
            flipCamera.setVisibility(View.VISIBLE);
        }
        if (!getBaseContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH)) {
            flashCameraButton.setVisibility(View.GONE);
        }


    }

    private class ImageAdapter extends BaseAdapter {
        private Context context;

        public ImageAdapter(Context localContext) {
            context = localContext;
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView picturesView;
            if (convertView == null) {
                picturesView = new ImageView(context);
                File f = new File(list.get(position));
                Picasso.with(context)
                        .load(f)
                        .resize(100, 100)
                        .centerCrop()
                        .into(picturesView);
                int wc = Resources.getSystem().getDisplayMetrics().widthPixels;
                int hc = wc / 4;
                picturesView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, hc));
            } else {
                picturesView = (ImageView) convertView;
                File f = new File(list.get(position));
                Glide.with(context).load(f).override(100,100).centerCrop().into(picturesView);
//                Picasso.with(context)
//                        .load(f)
//                        .resize(100, 100)
//                        .centerCrop()
//                        .into(picturesView);
            }
            return picturesView;
        }
    }

    private SlidingUpPanelLayout.PanelSlideListener onSlideListener() {
        return new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
                Log.i(TAG, "onPanelSlide: "+v);
                t1.setVisibility(View.VISIBLE);
                titleTextView.setVisibility(View.VISIBLE);
                sdcardImagesGridView.setPadding(0,getResources().getDimensionPixelSize(R.dimen.dp40),0,0 );
                if (v>=2)
                {
                    sdcardImagesGridView.setAlpha(1);
                    dragView.setAlpha(1);
                    t1.setAlpha(1);
                }else {
                    sdcardImagesGridView.setAlpha(v);
                    if (v>3) t1.setAlpha(v);
                    dragView.setAlpha(v);
                }

            }

            @Override
            public void onPanelCollapsed(View view) {
                t1.setVisibility(View.GONE);
                titleTextView.setVisibility(View.GONE);
                sdcardImagesGridView.setEnabled(false);
                sdcardImagesGridView.setAlpha(1);
                dragView.setAlpha(1);
                sdcardImagesGridView.setPadding(0,0,0,0 );
                t1.setAlpha(1);


            }

            @Override
            public void onPanelExpanded(View view) {

                sdcardImagesGridView.setEnabled(true);
                slidingLayout.setEnabled(true);
                RelativeLayout t1 = (RelativeLayout) findViewById(R.id.t1);
                slidingLayout.setDragView(t1);


            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {
            }
        };
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!openCamera(CameraInfo.CAMERA_FACING_BACK)) {
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        Log.i("h", "surfaceDestroyed: ");
    }


    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);
                camera.setErrorCallback(new ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }

        return result;

    }

    private void setUpCamera(Camera c) {
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }
        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }

        c.setDisplayOrientation(rotation);

        Parameters params = c.getParameters();

        List<String> i = camera.getParameters().getSupportedFocusModes();
        params.setFocusMode(i.get(i.size() - 1));
        showFlashButton(params);

        List<Camera.Size> sizes = params.getSupportedPreviewSizes();
        Camera.Size size = getBestPreviewSize(surfaceView.getWidth(), surfaceView.getHeight(),
                params);
        params.setPreviewSize(size.width, size.height);

        c.setParameters(params);

        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {

                params.setFlashMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }
    }

    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    private void showFlashButton(Parameters params) {
        boolean showFlash = (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH) && params.getFlashMode() != null)
                && params.getSupportedFlashModes() != null;

        flashCameraButton.setVisibility(showFlash ? View.VISIBLE
                : View.INVISIBLE);

    }

    private void flipCamera() {
        int id = (cameraId == CameraInfo.CAMERA_FACING_BACK ? CameraInfo.CAMERA_FACING_FRONT
                : CameraInfo.CAMERA_FACING_BACK);
        if (!openCamera(id)) {
//            alertCameraDialog();
        }
    }

    private void flashOnButton() {
        if (camera != null) {
            try {
                Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Parameters.FLASH_MODE_TORCH
                        : Parameters.FLASH_MODE_OFF);

                camera.setParameters(param);
                flashmode = !flashmode;
            } catch (Exception e) {
                Log.d("flashOnButton", e.getMessage());
            }

        }
    }

    private void cancel() {
        final Animation slideDownAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        slideDownAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation) {
                saveButtonLayout.setVisibility(View.GONE);
                capturedImageView.setVisibility(View.GONE);
                openCamera(cameraId);
                slidingLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        saveButtonLayout.startAnimation(slideDownAnimation);
    }

    private void openProfile() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flashButton:
                flashOnButton();
                break;
            case R.id.flipCameraButton:
                flipCamera();
                break;
            case R.id.captureButton:
                if (!isCaptured) {
                    isCaptured = true;
                    takeImage();
                }
                break;
            case R.id.profile:
                openProfile();
                break;
            case R.id.save:
                if (takenpicture!=null) new CameraActivity.TakeImage(takenpicture).execute();

                Intent i = new Intent(CameraActivity.this, LoadAnimActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.cancel:
                openCamera(CameraInfo.CAMERA_FACING_BACK);
                cancel();
                break;

            default:
                break;
        }
    }

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }

    private void takeImage() {
        final Animation slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        try {
            camera.takePicture(null, null, new PictureCallback() {

                @Override
                public void onPictureTaken(byte[] data, Camera camera) {

                    takenpicture = data;

                    Bitmap loadedImage = BitmapFactory.decodeByteArray(data, 0,
                            data.length);
                    Bitmap rotatedBitmap;
                    // rotate Image
                    Matrix rotateMatrix = new Matrix();
                    if (cameraId == CameraInfo.CAMERA_FACING_BACK) {
                        rotateMatrix.postRotate(rotation);
                    } else {
                        rotateMatrix.postRotate(270);
                    }
                    Log.i("rotation", "onPictureTaken: " + rotation);
                    rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                            loadedImage.getWidth(), loadedImage.getHeight(),
                            rotateMatrix, false);


                    capturedImageView.setImageBitmap(rotatedBitmap);
                    saveButtonLayout.setVisibility(View.VISIBLE);
                    capturedImageView.setVisibility(View.VISIBLE);
                    slidingLayout.setVisibility(View.GONE);
                    releaseCamera();
                    saveButtonLayout.setAnimation(slideUpAnimation);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        isCaptured = false;

    }

    private class TakeImage extends AsyncTask<Void, Void, Void> {
        private byte[] data;

        TakeImage(byte[] data) {
            this.data = data;
        }

        protected void onPostExecute(Void result) {

        }

        @Override
        protected Void doInBackground(Void... nothing) {
            File imageFile;
            // convert byte array into bitmap

            try {
                Bitmap loadedImage = null;
                Bitmap rotatedBitmap = null;
                loadedImage = BitmapFactory.decodeByteArray(data, 0,
                        data.length);
                // rotate Image
                Matrix rotateMatrix = new Matrix();
                rotateMatrix.postRotate(rotation);
                rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                        loadedImage.getWidth(), loadedImage.getHeight(),
                        rotateMatrix, false);
                String state = Environment.getExternalStorageState();
                File folder = null;
                if (state.contains(Environment.MEDIA_MOUNTED)) {
                    folder = new File(Environment
                            .getExternalStorageDirectory() + "/DCIM/Camera");
                } else {
                    folder = new File(Environment
                            .getExternalStorageDirectory() + "/DCIM/Camera");
                }

                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                if (success) {
                    java.util.Date date = new java.util.Date();
                    imageFile = new File(folder.getAbsolutePath()
                            + File.separator
                            + new Timestamp(date.getTime()).toString()
                            + "Image.jpg");

                    imageFile.createNewFile();
                } else {
                    Toast.makeText(getBaseContext(), "Image Not saved",
                            Toast.LENGTH_SHORT).show();
                    return null;
                }

                ByteArrayOutputStream ostream = new ByteArrayOutputStream();
                // save image into gallery
                rotatedBitmap.compress(CompressFormat.JPEG, 100, ostream);

                FileOutputStream fout = new FileOutputStream(imageFile);
                fout.write(ostream.toByteArray());
                fout.close();
                file=imageFile.getAbsolutePath();

                //for next retrival storing entry in db
                ContentValues values = new ContentValues();
                values.put(Images.Media.DATE_TAKEN,
                        System.currentTimeMillis());
                values.put(Images.Media.MIME_TYPE, "image/jpeg");
                values.put(MediaStore.MediaColumns.DATA,
                        imageFile.getAbsolutePath());
                CameraActivity.this.getContentResolver().insert(
                        Images.Media.EXTERNAL_CONTENT_URI, values);
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            return null;
        }
    }


}

