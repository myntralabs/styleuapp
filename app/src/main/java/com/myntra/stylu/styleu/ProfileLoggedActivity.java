package com.myntra.stylu.styleu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileLoggedActivity extends AppCompatActivity implements Imageutils.ImageAttachmentListener{



    CircleImageView profilepic;
    ImageView profileicon;
    TextView addphoto;
    ImageButton addicon;
    EditText addname;

    public static final String PROFILE_USER_ID = "USER_ID";
    public static final String PROFILE_FIRST_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_LAST_NAME = "PROFILE_LAST_NAME";
    public static final String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    String profileFirstName,profileLastName,profileimg;

    private Bitmap bitmap;
    private String file_name;

    Imageutils imageutils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        FacebookSdk.sdkInitialize(getApplicationContext());


        setContentView(R.layout.activity_profile_logged);

        imageutils =new Imageutils(this);

        profileicon= (ImageView) findViewById(R.id.profileicon);
        addphoto= (TextView) findViewById(R.id.addpictext);
        addicon= (ImageButton) findViewById(R.id.plus_icon);

        addname= (EditText) findViewById(R.id.text1);
        profilepic= (CircleImageView) findViewById(R.id.profile_pic);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            profileFirstName = extras.getString(ProfileRegisterActivity.PROFILE_FIRST_NAME);
            profileLastName = extras.getString(ProfileRegisterActivity.PROFILE_LAST_NAME);
            profileimg=extras.getString(ProfileRegisterActivity.PROFILE_IMAGE_URL);
            if (!profileimg.equals("")){
            Picasso.with(this).load(profileimg).into(profilepic);
                profileicon.setVisibility(View.GONE);
                addphoto.setVisibility(View.GONE);
            }
            else
            {
                profileicon.setVisibility(View.VISIBLE);
                addphoto.setVisibility(View.VISIBLE);
            }
            addname.setText(profileFirstName+" "+profileLastName);

        }


    profilepic= (CircleImageView) findViewById(R.id.profile_pic);

        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(ProfileLoggedActivity.this,"Chose pic",Toast.LENGTH_SHORT).show();
                imageutils.imagepicker(1);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        imageutils.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        imageutils.request_permission_result(requestCode, permissions, grantResults);
    }

    @Override
    public void image_attachment(int from, String filename, Bitmap file, Uri uri) {
        this.bitmap=file;
        this.file_name=filename;

        profileicon.setVisibility(View.GONE);
        addphoto.setVisibility(View.GONE);
       // Picasso.with(ProfileLoggedActivity.this).load(profileimg).into(profileView)
        profilepic.setImageBitmap(file);
        addicon.setVisibility(View.VISIBLE);
        addname.setText(profileFirstName + " " + profileLastName);
        String path =  Environment.getExternalStorageDirectory() + File.separator + "ImageAttach" + File.separator;
        imageutils.createImage(file,filename,path,false);

    }
}


