package com.myntra.stylu.styleu;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;


public class GalleryGridFragment extends Fragment {
    ArrayList<String> list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery_grid, container, false);

        list = ImagesFromDevice.getAllShownImagesPath(getContext());
        GridView sdcardImagesGridView = (GridView) view.findViewById(R.id.sdcard);
        sdcardImagesGridView.setAdapter(new ImageAdapter(getContext()));
        sdcardImagesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(getContext(), ""+position, Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private class ImageAdapter extends BaseAdapter {
        private Context context;

        public ImageAdapter(Context localContext) {
            context = localContext;
        }

        public int getCount() {
            return list.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView picturesView;
            if (convertView == null) {
                picturesView = new ImageView(context);
                File f = new File(list.get(position));
                Picasso.with(context)
                        .load(f)
                        .resize(100, 100)
                        .centerCrop()
                        .into(picturesView);
                int wc = Resources.getSystem().getDisplayMetrics().widthPixels;
                int hc = wc / 4;
                picturesView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, hc));
//                picturesView.setImageURI(Uri.parse(list.get(position)));
//                picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                picturesView.setPadding(8, 8, 8, 8);
//                picturesView.setScaleType(ImageView.ScaleType.MATRIX);

            } else {
                picturesView = (ImageView) convertView;
                File f = new File(list.get(position));
                Picasso.with(context)
                        .load(f)
                        .resize(100, 100)
                        .centerCrop()
                        .into(picturesView);
            }
            return picturesView;
        }
    }


}
