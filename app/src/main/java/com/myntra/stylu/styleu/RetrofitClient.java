package com.myntra.stylu.styleu;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by 300012406 on 27/09/17.
 */

public class RetrofitClient {
    static final String BASE_URL ="";
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static ApIService getApiService()
    {
        return RetrofitClient.getClient(BASE_URL).create(ApIService.class);
    }
}
