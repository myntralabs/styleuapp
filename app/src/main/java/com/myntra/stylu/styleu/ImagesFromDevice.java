package com.myntra.stylu.styleu;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by 300012406 on 29/09/17.
 */

public class ImagesFromDevice {

    public static ArrayList<String> getAllShownImagesPath(Context context) {
        ArrayList<String> listOfAllImages = new ArrayList<String>();
        Uri uri;
        Cursor cursor;
        int column_index_data;

        String absolutePathOfImage = null;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.TITLE,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.MIME_TYPE,
                MediaStore.Images.ImageColumns.SIZE,
                MediaStore.Images.ImageColumns.DATE_TAKEN};
        String date= MediaStore.Images.ImageColumns.DATE_TAKEN+" DESC";

        cursor = context.getContentResolver().query(uri, projection, null,
                null, date);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(absolutePathOfImage);
            Log.i("getAllShownImagesPath", absolutePathOfImage);
        }
        return listOfAllImages;
    }

}
