package com.myntra.stylu.styleu;

/**
 * Created by 300012407 on 03/10/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

    public class BadgeAdapter extends RecyclerView.Adapter<BadgeAdapter.MyViewHolder> {

        private Context mContext;
        private List<Badge_view> badgeList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            public ImageView thumbnail;

            public MyViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title);
                thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            }
        }


        public BadgeAdapter(Context mContext, List<Badge_view> badgeList) {
            this.mContext = mContext;
            this.badgeList = badgeList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.badge_view, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            Badge_view album = badgeList.get(position);
            holder.title.setText(album.getName());
            //holder.count.setText(album.getNumOfSongs() + " songs");

            // loading album cover using Glide library
//            Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);


        }
        @Override
        public int getItemCount() {
            return badgeList.size();
        }

    }