package com.myntra.stylu.styleu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileLogged2Activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BadgeAdapter adapter;
    private List<Badge_view> badgeList;
    Toolbar toolbar;
    ImageButton camrea_white,camrea_black,setting_black,setting_white,backarrow_black,backarrow_white;
    CircleImageView profileicon_small;
    TextView username;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setContentView(R.layout.activity_profile_logged2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        backarrow_black= (ImageButton) findViewById(R.id.back_arrow_black);
        backarrow_white= (ImageButton) findViewById(R.id.back_arrow);
        camrea_black= (ImageButton) findViewById(R.id.camera_icon_black);
        camrea_white= (ImageButton) findViewById(R.id.camera_icon);
        setting_black= (ImageButton) findViewById(R.id.settings_black);
        setting_white= (ImageButton) findViewById(R.id.settings);
        profileicon_small= (CircleImageView) findViewById(R.id.profile_icon_small);
        username= (TextView) findViewById(R.id.user_name);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        badgeList = new ArrayList<>();
        adapter = new BadgeAdapter(this, badgeList);


//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();

    }



    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);
        final RelativeLayout collapsingContent= (RelativeLayout) findViewById(R.id.collapsingContent);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    collapsingContent.setVisibility(View.INVISIBLE);
                    toolbar.setBackgroundColor(Color.parseColor("#F7ffffff"));
                    setting_white.setVisibility(View.GONE);
                    camrea_white.setVisibility(View.GONE);
                    backarrow_white.setVisibility(View.GONE);
                    setting_black.setVisibility(View.VISIBLE);
                    backarrow_black.setVisibility(View.VISIBLE);
                    camrea_black.setVisibility(View.VISIBLE);
                    profileicon_small.setVisibility(View.VISIBLE);
                    username.setVisibility(View.VISIBLE);

                    isShow = true;
                } else if (isShow) {
                    //collapsingToolbar.setTitle("StyleU");
                    collapsingContent.setVisibility(View.VISIBLE);
                    setting_white.setVisibility(View.VISIBLE);
                    camrea_white.setVisibility(View.VISIBLE);
                    backarrow_white.setVisibility(View.VISIBLE);
                    setting_black.setVisibility(View.GONE);
                    backarrow_black.setVisibility(View.GONE);
                    camrea_black.setVisibility(View.GONE);
                    profileicon_small.setVisibility(View.GONE);
                    username.setVisibility(View.GONE);
                    toolbar.setBackgroundColor(Color.TRANSPARENT);
                    isShow = false;
                }
            }
        });
    }
    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.aaa,
                R.drawable.aaa,
                R.drawable.aaa,
                R.drawable.aaa,
                R.drawable.aaa};

        Badge_view a = new Badge_view("Enrique", 13, covers[0]);
        badgeList.add(a);

        a = new Badge_view("Xscpae", 8, covers[1]);
        badgeList.add(a);

        a = new Badge_view("Maroon 5", 11, covers[2]);
        badgeList.add(a);

        a = new Badge_view("Born to Die", 12, covers[3]);
        badgeList.add(a);

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            /*this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;*/
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
          /*  int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }*/
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}