package com.myntra.stylu.styleu;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileRegisterActivity extends AppCompatActivity {


    private SignInButton goobleSignInButton;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int GOOGLE_SIGN_IN = 100;

    private static final String TAG = "Register";
    LinearLayout ll_img;
    ImageView img_back;
    LoginButton fblogin;
    ImageButton googlelogin, twitterlogin;

    private CallbackManager mCallbackManager;
    public static final String PROFILE_USER_ID = "USER_ID";
    public static final String PROFILE_FIRST_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_LAST_NAME = "PROFILE_LAST_NAME";
    public static final String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        FacebookSdk.sdkInitialize(ProfileRegisterActivity.this);
        setContentView(R.layout.activity_profile_register);

//        google code
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        goobleSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
        goobleSignInButton.setSize(SignInButton.SIZE_WIDE);
        goobleSignInButton.setScopes(googleSignInOptions.getScopeArray());
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
        goobleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                //Starting intent for result
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
            }
        });

//        facebook code
//        fblogin = (ImageButton) findViewById(R.id.facebook_login);

        mCallbackManager = CallbackManager.Factory.create();
        fblogin = (LoginButton) findViewById(R.id.facebook_login);
        fblogin.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                String userLoginId = loginResult.getAccessToken().getUserId();
                Intent facebookIntent = new Intent(ProfileRegisterActivity.this, ProfileLoggedActivity.class);
                Profile mProfile = Profile.getCurrentProfile();
                String firstName = mProfile.getFirstName();
                String lastName = mProfile.getLastName();
                String userId = mProfile.getId().toString();
                String profileImageUrl = mProfile.getProfilePictureUri(500, 500).toString();

                Log.i(TAG, "onSuccess: " + mProfile.getFirstName() + " \n");
                facebookIntent.putExtra(PROFILE_USER_ID, userId);
                facebookIntent.putExtra(PROFILE_FIRST_NAME, firstName);
                facebookIntent.putExtra(PROFILE_LAST_NAME, lastName);
                facebookIntent.putExtra(PROFILE_IMAGE_URL, profileImageUrl);
                startActivity(facebookIntent);
            }

            @Override
            public void onCancel() {
                Toast.makeText(ProfileRegisterActivity.this, "Cancle", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(ProfileRegisterActivity.this, "Error", Toast.LENGTH_SHORT).show();

            }
        });

        twitterlogin = (ImageButton) findViewById(R.id.twitter_login);
        twitterlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutGoogle();
            }
        });
        ll_img = (LinearLayout) findViewById(R.id.ll_background);
        img_back = (ImageView) findViewById(R.id.background_img);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void logoutGoogle() {
        if (mGoogleApiClient == null) return;
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                Toast.makeText(ProfileRegisterActivity.this, "out", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleSignInResultGoogle(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            Intent facebookIntent = new Intent(ProfileRegisterActivity.this, ProfileLoggedActivity.class);

            String firstName = acct.getDisplayName() == null ? "" : acct.getDisplayName();
            String lastName = "";
            String profileImageUrl = acct.getPhotoUrl() == null ? "" : acct.getPhotoUrl().toString();

            Log.i(TAG, "onSuccess: " + firstName + " \n");

            facebookIntent.putExtra(PROFILE_FIRST_NAME, firstName);
            facebookIntent.putExtra(PROFILE_LAST_NAME, lastName);
            facebookIntent.putExtra(PROFILE_IMAGE_URL, profileImageUrl);
            startActivity(facebookIntent);


            //Initializing image loader

        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResultGoogle(result);
        }
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    protected void onResume() {
        super.onResume();

        //fb
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //fb
        AppEventsLogger.deactivateApp(this);
    }
}

